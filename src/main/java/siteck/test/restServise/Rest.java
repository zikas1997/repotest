package siteck.test.restServise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import siteck.test.model.Respose;
import siteck.test.parsers.xml.ParseXmlXpath;


/**
 * Created by Ziaks on 21.09.2017.
 */
@RestController
public class Rest {

    //////////////////////////////////
    //Класс отвечающий за REST запрос
    //////////////////////////////////
    private  static Logger logger = LoggerFactory.getLogger(Rest.class);

    @RequestMapping("/student")
    public Respose reply(@RequestParam(value="nameTag", defaultValue="This tag is missing") String name) {

        ParseXmlXpath parseXmlXpath = new ParseXmlXpath(name);
        logger.debug("http://localhost:8080/student?nameTag=",name);
        return new Respose(String.format(parseXmlXpath.parseListTag()));

    }

}
