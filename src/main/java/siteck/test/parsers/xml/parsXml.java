package siteck.test.parsers.xml;

import java.util.List;

/**
 * Created by Ziaks on 21.09.2017.
 */
public interface parsXml {
    String parseAll();
    String parseTag();
    String parseListTag();
}
