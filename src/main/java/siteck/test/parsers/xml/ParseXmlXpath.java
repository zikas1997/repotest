package siteck.test.parsers.xml;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by Ziaks on 21.09.2017.
 */
public class ParseXmlXpath implements parsXml{

    ////////////////////////////////
    //Класс для парсинга xml файла
    ///////////////////////////////


    private String nameTag;
    private String response;
    private  static Logger logger = LoggerFactory.getLogger(ParseXmlXpath.class);

    public ParseXmlXpath(String nameTag) {
        this.nameTag = nameTag;
    }

    @Override
    public String parseAll() {
        try {

            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = documentBuilder.parse("src/main/resources/Student.xml");

            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();

            XPathExpression expr = xpath.compile("/");

            Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);

            getAllNodeFromThisNode(node);
        }
        catch (ParserConfigurationException | IOException e){
            logger.error("File not found!",e);
        }
        catch (XPathExpressionException e) {
            logger.error("XPath is not initialized.",e);
        } catch (SAXException e) {
            logger.error("SAXException is not initialized.",e);
        }
        return response;
    }

    @Override
    public String parseTag() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder;
        Document doc = null;
        try {

            builder = factory.newDocumentBuilder();
            doc = builder.parse("src/main/resources/Student.xml");

            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();

            XPathExpression expr =
                    xpath.compile("//"+nameTag+ "/text()");
            response = (String) expr.evaluate(doc, XPathConstants.STRING);
        }
        catch (ParserConfigurationException | IOException e){
            logger.error("File not found!",e);
        }
        catch (SAXException e) {
            logger.error("SAXException is not initialized.",e);
        } catch (XPathExpressionException e) {
            logger.error("XPath is not initialized.",e);
        }
        return response;
   }

    @Override
    public String parseListTag() {

        try {

            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = documentBuilder.parse("src/main/resources/Student.xml");

            XPathFactory xpathFactory = XPathFactory.newInstance();
            XPath xpath = xpathFactory.newXPath();

            XPathExpression expr = xpath.compile("//"+nameTag);

            Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);
            getAllNodeFromThisNode(node);
        }
        catch (ParserConfigurationException | IOException e){
            logger.error("File not found!",e);
        }
        catch (XPathExpressionException e) {
            logger.error("XPath is not initialized.",e);
        } catch (SAXException e) {
            logger.error("SAXException is not initialized.",e);
        }
        return response;
    }
    private void getAllNodeFromThisNode(Node node){
        try{

            StreamResult xmlOutput = new StreamResult(new StringWriter());
            Transformer transformer = TransformerFactory.newInstance().newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.transform(new DOMSource(node), xmlOutput);

            response = xmlOutput.getWriter().toString();
            logger.debug("Done");

        }
        catch (TransformerFactoryConfigurationError|TransformerException throwable){
            logger.error("File not found!");
        }
    }

}
