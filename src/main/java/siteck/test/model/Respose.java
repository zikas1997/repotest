package siteck.test.model;

/**
 * Created by Ziaks on 21.09.2017.
 */
public class Respose {

    ///////////////////////////////////
    //Класс модель для ответа
    ///////////////////////////////////

    private final String response;

    public Respose(String response) {
        this.response = response;
    }

    public String getResponse() {
        return response;
    }

}
